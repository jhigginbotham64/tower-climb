extends Area2D

# ok kids, this is an idea i had after brainstorming how to implement ladders
# and ropes. you can't just say "i can move if i overlap a traversable area"
# because you need to be able to smoothly get back on if you leave, as well
# as transition smoothly between adjacent areas. this connector is used to
# indicate the boundaries of different areas, as well as which direction the
# next area is supposed to be in. it currently has a default area of 4 pixels,
# which should be precise enough for any practical application.

export(Vector2) var CONNECTION_DIR

func _ready() -> void:
  pass
