extends Node2D

# IMPORTANT NOTES:
# - _ready is called after the node *and its children* have _all_ entered the
#   scene tree, so you shouldn't use it when what you mean is _enter_tree, which
#   is called _before any children_ have entered the tree. also _exit_tree is
#   called _after all children_ have left the tree.
# - Node.get_path_to(other_node) is available for relative paths between nodes
#   *in the same scene*
# - get_node_or_null is a thing
# - you should really think about using _process instead of _physics_process
#   in some places
# - and also about "unsetting player input" in either the player's _process
#   or _physics_process functions
# - the delete function is Node.queue_free()
# - Node.print_tree() and Node.print_tree_pretty() are both available
# - signals could greatly simplify this process. in fact decent "co-dependent
#   lifecycle functions" are really only possible with signals. think of React.js.
# - okay, i now think that levels should be started via a "play" function
#   after the scene tree has been set up.

var next_scene_loaded = false
var current_scene = ""
var next_scene = game.room_names.DEBUG_ARENA

var cameras = {}
var playing_camera = null

func _on_load_request(args):
  if args == game.load_requests.NEXT_SCENE:
    if current_scene == game.room_names.DEBUG_ARENA:
      next_scene = game.room_names.DEBUG_ARENA
      call_deferred("add_camera", next_scene)
      call_deferred("setup_scene", next_scene)

func _on_game_over(args):
  pass

func _on_scene_end(args):
  if current_scene == game.room_names.DEBUG_ARENA:
    pass

func _on_scene_ready(args):
  if current_scene == "":
    if args is Vector2:
      if next_scene == game.room_names.DEBUG_ARENA:
        cameras[next_scene].tint_cover(Color.black)
        cameras[next_scene].fade_in_to(args)
        call_deferred("action", next_scene)
  else:
    if args is Vector2:
      # at this point the new scene is "ready to play"
      # but the old scene is not necessarily "ready to cut",
      # so start dissolving and we'll figure out later when
      # the old scene is ready to stop
      if current_scene == game.room_names.DEBUG_ARENA and next_scene == game.room_names.DEBUG_ARENA:
        playing_camera.dissolve_out()
        # this function needs to handle storing information about the
        # next scenes transition-in
        # in our case the transition is stupidly simple, but it still
        # needs to be done
        cameras[next_scene].jump_to(args)
        next_scene_loaded = true


# ok, uh...so what exactly does each of these things do, and furthermore,
# wut does the camera need to do?
# 1. lights() -> "lights on" for level: camera loads requested level
# 2. camera() -> camera transition in
# 3. action() -> finish other setup and play scene
# 4. cut() -> camera transition out

func add_camera(scene):
  var cam = game.camera.instance()
  cameras[scene] = cam

func setup_scene(scene):
  var cam = cameras[scene]
  add_child(cam)
  move_child(cam, 0)
  cam.add_scene(scene)
  cam.roll()

func action(scene):
  if playing_camera != null:
    playing_camera.cut()
    remove_child(playing_camera)
    playing_camera.queue_free()
  var cam = cameras[scene]
  cameras.erase(scene)
  playing_camera = cam
  current_scene = scene
  if scene == next_scene:
    next_scene = ""
  cam.play_scene()

func _ready() -> void:
  game.connect_game_signal(game.signals.LOAD, self, "_on_load_request")
  game.connect_game_signal(game.signals.GAME_OVER, self, "_on_game_over")
  game.connect_game_signal(game.signals.SCENE_READY, self, "_on_scene_ready")
  game.connect_game_signal(game.signals.SCENE_READY, self, "_on_scene_end")

  add_camera(next_scene)
  setup_scene(next_scene)

func _process(delta: float) -> void:
  # you know, i could just define some camera signals, and
  # that might save me a lot of trouble. but right now i
  # just wanna get stuff implemented, improvements can wait
  # until i have a better vision of what exactly they need
  # to be
  if next_scene_loaded and playing_camera.is_dissolved_out():
    action(next_scene)
    if cameras.size() == 0:
      next_scene_loaded = false


# ok so a scene is playing
# then that scene requests that the next scene be loaded
# we add the new camera and setup the new scene
# the new scene will get a signal, _on_scene_ready, when
# it can be played. the current implementation has the current
# scene begin dissolving out as soon as the load request is
# received, but the dissolve-out should not begin until the
# next scene has received _on_scene_ready.
# the old scene will send a signal, _on_scene_end, when
# it can be stopped and the next scene played
# when the new scene is ready,