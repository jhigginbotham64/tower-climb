extends Node2D

onready var screen_size = Vector2( \
  ProjectSettings.get("display/window/size/width"), \
  ProjectSettings.get("display/window/size/height"))

onready var cam2d = find_node("Camera2D")
onready var cmod = find_node("CanvasModulate")
onready var view = find_node("view")
onready var cover = find_node("cover")
onready var frame = find_node("frame")
onready var lens = find_node("lens")
onready var set = find_node("set")
onready var aim = find_node("aim")
var scene = null # variable set by setup_scene then added as child of set

var SCROLL_SPEED = game.scroll_speed
var SCROLL_VEC = Vector2(0,0)
var FOLLOWING = false
var FOLLOW_INITIALIZED = false
var FOLLOW_TARGET = null
var FOLLOW_CELL = null
var HORIZONTAL_BUFFER = 0
var VERTICAL_BUFFER = 0

enum FADE_SPEED { NEVER=0, SLOWEST=25, SLOWER=50, SLOW=75, BELOW_AVERAGE, AVERAGE=125, ABOVE_AVERAGE=150, FAST=175, FASTER=200, FASTEST=225, INSTANT=255 }
var FADING_SPEED = FADE_SPEED.FASTER
const LENS_COVER_SIZE_MULT = 4

# dissolves use similar constructs as fades,
# but they're not the same thing so the code
# reads really strange if their stuff is named
# the same
enum DISSOLVE_SPEED { NEVER=0, SLOWEST=25, SLOWER=50, SLOW=75, BELOW_AVERAGE, AVERAGE=125, ABOVE_AVERAGE=150, FAST=175, FASTER=200, FASTEST=225, INSTANT=255 }
var CMOD_DISSOLVING_SPEED = DISSOLVE_SPEED.FASTER
var POST_DISSOLVING_SPEED = DISSOLVE_SPEED.FASTER

# expressing alpha targets in terms of targets, instead of min/max
# and direction, allows for much greater control of camera functions
var COVER_ALPHA_TARGET = game.alpha_min
var CMOD_ALPHA_TARGET = game.alpha_max
var POST_ALPHA_TARGET = game.alpha_max

func _ready():

  # start off centered to "default viewport"
  #position = screen_size / 2
  #set.position = screen_size / 2
  #aim.position = screen_size / 2


  # not sure whether this is the most effective way to structure
  # this scene, but obviously the *actual screen center* needs to
  # be independent of the parent's transform. this is especially
  # important for managing the so-called "lens cover".
  frame.rect_size = screen_size
  frame.visible = true

  # need it to be bigger than the screen so that its position
  # adjustments don't have to be pixel-perfect on every frame,
  # otherwise it appears to "slide out of place" or be slightly
  # behind the actual camera movement
  cover.rect_position -= screen_size * LENS_COVER_SIZE_MULT
  cover.rect_size = screen_size * LENS_COVER_SIZE_MULT * 2

# sticking with _phsyics_process over just _process, for
# movement specifically, since what's on-screen can determine
# world state, which needs to be more-or-less independent of frame rate.
func _physics_process(delta):

  # screen scrolling and camera follow are a bit intertwined
  if SCROLL_VEC.length() != 0:
    get_tree().paused = true
    var frame_scroll_vec = SCROLL_VEC.normalized() * SCROLL_SPEED * delta
    # check for scrolling past end of scroll target
    if frame_scroll_vec.length() >= SCROLL_VEC.length():
      frame_scroll_vec = SCROLL_VEC
      SCROLL_VEC = Vector2(0,0)
    else:
      SCROLL_VEC -= frame_scroll_vec
    jump(frame_scroll_vec)
  elif FOLLOWING and not FOLLOW_INITIALIZED and not get_tree().paused:
    FOLLOW_TARGET.remote_path = "../../../../aim"
    var shape = FOLLOW_CELL.find_node("CollisionShape2D").shape
    cam2d.limit_left = FOLLOW_CELL.position.x - shape.extents.x - HORIZONTAL_BUFFER
    cam2d.limit_right = FOLLOW_CELL.position.x + shape.extents.x + HORIZONTAL_BUFFER
    cam2d.limit_top = FOLLOW_CELL.position.y - shape.extents.y - VERTICAL_BUFFER
    cam2d.limit_bottom = FOLLOW_CELL.position.y + shape.extents.y + VERTICAL_BUFFER
    cam2d.current = true
    FOLLOW_INITIALIZED = true
  else:
    get_tree().paused = false

  # NOTE legacy code kept around in case i remember what problem
  # it solved in the past
  # keeping track of the center of the screen, because
  # i find myself needing it quite a lot
#  if FOLLOWING and not get_tree().paused:
#    view.position = cam2d.get_camera_screen_center()
#  else:
#    view.position = position

# visual-only effects can be frame-rate-dependent
func _process(delta):
  # fading effects
  if cover_get_alpha() != COVER_ALPHA_TARGET:
    cover_shift_alpha(game.get_sign(COVER_ALPHA_TARGET - cover_get_alpha()) * FADING_SPEED * delta)

  # dissolves
  # distinction is made between "post-processing dissolves", which apply gradual
  # transparency to the rendered scene via the viewport container's modulate,
  # and "canvas modulate dissolves", which apply gradual transparency to each
  # separate item on the canvas via a CanvasModulate node within the viewport
  if post_get_alpha() != POST_ALPHA_TARGET:
    post_shift_alpha(game.get_sign(POST_ALPHA_TARGET - post_get_alpha()) * POST_DISSOLVING_SPEED * delta)

  if cmod_get_alpha() != CMOD_ALPHA_TARGET:
    cmod_shift_alpha(game.get_sign(CMOD_ALPHA_TARGET - cmod_get_alpha()) * CMOD_DISSOLVING_SPEED * delta)

## SCENE LOADING
# since i'm running with the idea of scenes being "in front of the camera",
# the camera allows higher-level scenes to tell it what scene to display
# in addition to providing an API for lower-level scenes to control camera
# movement appropriately. this allows precise control of transitions between
# scenes, while still alowing individual scenes to retain their "unique
# character".

func add_scene(scene_name: String=game.rooms.keys()[0]):
  scene = game.rooms[scene_name].instance()
  set.add_child(scene)

func roll():
  scene.setup(self)

func play_scene():
  scene.play()

func cut():
  if scene != null:
    set.remove_child(scene)
    scene.queue_free()

## CAMERA MOVEMENT

# NOTE
# most camera movements imply that the camera is no longer "following",
# but calling one while following causes weird, non-intuitive behavior,
# so i...make sure manually, which is weird and clutzy but...technically
# correct. idk if i could refactor it to be nicer. rn imma not think about
# it too much.

# hard-set camera to new position, named "jump"
# after the similar-looking "jump cut".
# this function may switch implementations, to moving aim.position
# and thereby the child Camera2D, which would mean not directly
# manipulating the camera transform.
# alternatively, it may switch to manipulating the canvas transform
# via aim.position and otherwise staying the same, switching the
# Camera2D active/inactive as needed like it currently does.
func jump(vec: Vector2=Vector2()):
  unfollow()
  aim.position += vec
  # toto, i don't think we're manipulating the
  # root viewport's canvas transform any more
  #var trans = get_viewport().get_canvas_transform()
  var trans = lens.get_canvas_transform()
  trans.origin = -aim.position + (screen_size / 2)
  #get_viewport().set_canvas_transform(trans)
  lens.set_canvas_transform(trans)

func jump_to(pos: Vector2=aim.position):
  unfollow()
  jump(pos - aim.position)

# move camera to from position to end of vec at speed
func scroll(vec: Vector2=Vector2(), speed: int=game.scroll_speed):
  unfollow()
  SCROLL_SPEED = speed
  SCROLL_VEC = vec

func scroll_to(pos: Vector2=aim.position, speed: int=game.scroll_speed):
  unfollow()
  scroll(pos - aim.position, speed)

# first-draft camera follow implementation makes a *lot* of (perhaps unwarranted)
# assumptions, most especially with regards to node paths: it assumes that the
# camera and target have the same parent, and that the target's RemoteTransform2D
# is a first-level child. the idea of buffers is also very basic and stems from
# the original use-case of strictly rectangular camera follow zones. but you
# know what they say, "it works for now"...
# also if it accidentally gets called twice in a row, be sure to unfollow first
# so our starting state is consistent.
func follow(target: CanvasItem, cell: Area2D, horizontal_buffer: int=0, vertical_buffer: int=0):
  unfollow()
  FOLLOW_TARGET = target.find_node("RemoteTransform2D")
  FOLLOW_CELL = cell
  HORIZONTAL_BUFFER = horizontal_buffer
  VERTICAL_BUFFER = vertical_buffer
  FOLLOWING = true
  FOLLOW_INITIALIZED = false

func unfollow():
  if FOLLOWING and FOLLOW_INITIALIZED:
    FOLLOW_TARGET.remote_path = ""
    FOLLOW_TARGET = null
    FOLLOW_CELL = null
    HORIZONTAL_BUFFER = 0
    VERTICAL_BUFFER = 0
    FOLLOWING = false
    FOLLOW_INITIALIZED = false
    aim.position = cam2d.get_camera_screen_center()
    cam2d.current = false

## CAMERA EFFECTS

func tint_cover(color: Color=Color.black):
  cover.set_frame_color(color)

func cover_shift_alpha(ashift: int=0):
  var alpha = cover_get_alpha() + ashift
  if alpha != COVER_ALPHA_TARGET and \
    ((cover_get_alpha() < COVER_ALPHA_TARGET and alpha > COVER_ALPHA_TARGET) or \
    (cover_get_alpha() > COVER_ALPHA_TARGET and alpha < COVER_ALPHA_TARGET)):
    alpha = COVER_ALPHA_TARGET
  cover_set_alpha(alpha)

func cover_get_alpha():
  return cover.get_frame_color().a8

func cover_set_alpha(alpha: int=game.alpha_max):
  var color = cover.get_frame_color()
  color.a8 = game.bounded_alpha(alpha)
  cover.set_frame_color(color)

func cover_set_alpha_target(alpha: int=game.alpha_max):
  COVER_ALPHA_TARGET = game.bounded_alpha(alpha)

func fade(color: Color=Color.black, speed: int=FADE_SPEED.FASTER, target: int=game.alpha_max):
  color.a8 = cover_get_alpha()
  tint_cover(color)
  cover_set_alpha_target(target)
  FADING_SPEED = speed

func fade_in(speed: int=FADE_SPEED.FASTER):
  fade(Color.black, speed, game.alpha_min)

func fade_in_to(pos, speed: int=FADE_SPEED.FASTER):
  jump(pos)
  fade_in(speed)

func fade_out(speed: int=FADE_SPEED.FASTER):
  fade(Color.black, speed, game.alpha_max)

func fade_out_from(pos, speed: int=FADE_SPEED.FASTER):
  jump(pos)
  fade_out(speed)

func wash_in(speed: int=FADE_SPEED.FASTER):
  fade(Color.white, speed, game.alpha_min)

func wash_in_to(pos, speed: int=FADE_SPEED.FASTER):
  jump(pos)
  wash_in(speed)

func wash_out(speed: int=FADE_SPEED.FASTER):
  fade(Color.white, speed, game.alpha_max)

func wash_out_from(pos, speed: int=FADE_SPEED.FASTER):
  jump(pos)
  wash_out(speed)

func is_fading():
  return cover_get_alpha() == COVER_ALPHA_TARGET

func is_faded_in():
  return cover_get_alpha() == game.alpha_min

func is_faded_out():
  return cover_get_alpha() == game.alpha_max

func is_washing():
  return cover.get_frame_color() == Color.white and is_fading()

func is_washed_in():
  return cover.get_frame_color() == Color.white and is_faded_in()

func is_washed_out():
  return cover.get_frame_color() == Color.white and is_faded_out()

func tint_post(color: Color=Color.white):
  frame.set_self_modulate(color)

func post_shift_alpha(ashift: int=0):
  var alpha = post_get_alpha() + ashift
  if alpha != POST_ALPHA_TARGET and \
    ((post_get_alpha() < POST_ALPHA_TARGET and alpha > POST_ALPHA_TARGET) or \
    (post_get_alpha() > POST_ALPHA_TARGET and alpha < POST_ALPHA_TARGET)):
    alpha = POST_ALPHA_TARGET
  post_set_alpha(alpha)

func post_get_alpha():
  return frame.get_self_modulate().a8

func post_set_alpha(alpha: int=game.alpha_max):
  var color = frame.get_self_modulate()
  color.a8 = game.bounded_alpha(alpha)
  frame.set_self_modulate(color)

func post_set_alpha_target(alpha: int=game.alpha_max):
  POST_ALPHA_TARGET = game.bounded_alpha(alpha)

func dissolve_post(color: Color=Color.white, speed: int=DISSOLVE_SPEED.FASTER, target: int=game.alpha_max):
  color.a8 = post_get_alpha()
  tint_post(color)
  post_set_alpha_target(target)
  POST_DISSOLVING_SPEED = speed

func dissolve_in_post(speed: int=DISSOLVE_SPEED.FASTER):
  dissolve_post(Color.white, speed, game.alpha_max)

func dissolve_in_to_post(pos, speed: int=DISSOLVE_SPEED.FASTER):
  jump(pos)
  dissolve_in_post(speed)

func dissolve_out_post(speed: int=DISSOLVE_SPEED.FASTER):
  dissolve_post(Color.white, speed, game.alpha_min)

func dissolve_out_from_post(pos, speed: int=DISSOLVE_SPEED.FASTER):
  jump(pos)
  dissolve_out_post(speed)

func is_post_dissolving():
  return post_get_alpha() != POST_ALPHA_TARGET

func is_post_dissolved_in():
  return post_get_alpha() == game.alpha_max

func is_post_dissolved_out():
  return post_get_alpha() == game.alpha_min

func tint_cmod(color: Color=Color.white):
  cmod.set_color(color)

func cmod_shift_alpha(ashift: int=0):
  var alpha = cmod_get_alpha() + ashift
  if alpha != CMOD_ALPHA_TARGET and \
    ((cmod_get_alpha() < CMOD_ALPHA_TARGET and alpha > CMOD_ALPHA_TARGET) or \
    (cmod_get_alpha() > CMOD_ALPHA_TARGET and alpha < CMOD_ALPHA_TARGET)):
    alpha = CMOD_ALPHA_TARGET
  cmod_set_alpha(alpha)

func cmod_get_alpha():
  return cmod.get_color().a8

func cmod_set_alpha(alpha: int=game.alpha_max):
  var color = cmod.get_color()
  color.a8 = game.bounded_alpha(alpha)
  cmod.set_color(color)

func cmod_set_alpha_target(alpha: int=game.alpha_max):
  CMOD_ALPHA_TARGET = game.bounded_alpha(alpha)

func dissolve_cmod(color: Color=Color.white, speed: int=DISSOLVE_SPEED.FASTER, target: int=game.alpha_max):
  color.a8 = cmod_get_alpha()
  tint_cmod(color)
  cmod_set_alpha_target(target)
  CMOD_DISSOLVING_SPEED = speed

func dissolve_in_cmod(speed: int=DISSOLVE_SPEED.FASTER):
  dissolve_post(Color.white, speed, game.alpha_max)

func dissolve_in_to_cmod(pos, speed: int=DISSOLVE_SPEED.FASTER):
  jump(pos)
  dissolve_in_cmod(speed)

func dissolve_out_cmod(speed: int=DISSOLVE_SPEED.FASTER):
  dissolve_cmod(Color.white, speed, game.alpha_min)

func dissolve_out_from_cmod(pos, speed: int=DISSOLVE_SPEED.FASTER):
  jump(pos)
  dissolve_out_cmod(speed)

func is_cmod_dissolving():
  return cmod_get_alpha() != CMOD_ALPHA_TARGET

func is_cmod_dissolved_in():
  return cmod_get_alpha() == game.alpha_max

func is_cmod_dissolved_out():
  return cmod_get_alpha() == game.alpha_min

# default dissolves are post-dissolves
func dissolve(color: Color=Color.white, speed: int=DISSOLVE_SPEED.FASTER, target: int=game.alpha_max):
  dissolve_post(color, speed, target)

func dissolve_in(speed: int=DISSOLVE_SPEED.FASTER):
  dissolve_in_post(speed)

func dissolve_in_to(pos, speed: int=DISSOLVE_SPEED.FASTER):
  dissolve_in_to_post(pos, speed)

func dissolve_out(speed: int=DISSOLVE_SPEED.FASTER):
  dissolve_out_post(speed)

func dissolve_out_from(pos, speed: int=DISSOLVE_SPEED.FASTER):
  dissolve_out_from_post(pos, speed)

func is_dissolving():
  return is_post_dissolving()

func is_dissolved_in():
  return is_post_dissolved_in()

func is_dissolved_out():
  return is_post_dissolved_out()