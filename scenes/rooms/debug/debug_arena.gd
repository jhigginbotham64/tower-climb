extends Node2D

onready var zone_1 = $"./camera_zones/1"
onready var zone_2 = $"./camera_zones/2"
onready var zone_3 = $"./camera_zones/3"
onready var zone_4 = $"./camera_zones/4"

# fun story, levels use to have their cameras as children,
# and then i completely redid the camera implementation
# and have to pass it a reference to a parent camera
var cam = null

var current_cam_zone = null
var next_cam_zone = null
var player = null

# testing transparency stuff to get ready for cross-dissolve
#func _process(delta: float) -> void:
#  if current_cam_zone == zone_2:
#    if not cam.is_dissolved_out() and not cam.is_dissolving():
#      cam.dissolve_out()
#    elif cam.is_dissolved_out():
#      cam.dissolve_in()
#  else:
#    cam.dissolve_in()

func _ready():
  game.supplement_tiles(self)

func setup(running_camera: Node):
  cam = running_camera
  player = game.player.instance()
  var start_pos = zone_2.find_node("Position2D").get_global_transform().get_origin()
  player.position = start_pos + Vector2(0, -(game.base_size.y * 6))
  game.emit_game_signal(game.signals.SCENE_READY, start_pos)

func play():
  add_child(player)

func _on_camera_zone_entered(zone, area):
  if area.get_parent().is_in_group("player"):
    if current_cam_zone == null:
      # currently the only scenario where this block
      # should be executed is on level start
      current_cam_zone = zone
    else:
      next_cam_zone = zone

func _on_camera_zone_exited(zone, area):
  if area.get_parent().is_in_group("player"):
    if zone == next_cam_zone and current_cam_zone != null:
      next_cam_zone = null
    # basic transitions
    elif zone == current_cam_zone and next_cam_zone != null and current_cam_zone != next_cam_zone:
      transition()
    else:
      # not the most robsut way to do things, but works for now
      game.emit_game_signal(game.signals.LOAD, game.load_requests.NEXT_SCENE)

func transition():
  if current_cam_zone == zone_4:
    cam.wash_in()
  if current_cam_zone == zone_1:
    cam.fade(Color.blue, cam.FADE_SPEED.FASTER, game.alpha_min)

  # only execute defined transitions
  if next_cam_zone == zone_1:
    cam.scroll_to(zone_1.find_node("Position2D").get_global_transform().get_origin())
    cam.fade(Color.red, cam.FADE_SPEED.FASTER, game.alpha_max)
  elif next_cam_zone == zone_2:
    cam.scroll_to(zone_2.find_node("Position2D").get_global_transform().get_origin())
  elif next_cam_zone == zone_3:
    # need to figure out how to implement some kind of "follow zone"
    if current_cam_zone == zone_2:
      cam.scroll(Vector2(cam.screen_size.x - game.base_size.x,0))
    elif current_cam_zone == zone_4:
      cam.scroll(Vector2(0,-(cam.screen_size.y - game.base_size.y)))
    cam.follow(player, zone_3, game.base_size.x / 2, game.base_size.y / 2)
  elif next_cam_zone == zone_4:
    cam.scroll_to(zone_4.find_node("Position2D").get_global_transform().get_origin())
    cam.wash_out()
  else:
    pass

  # set up for next round of stuff
  current_cam_zone = next_cam_zone
  next_cam_zone = null

func _on_1_area_entered(area: Area2D) -> void:
  _on_camera_zone_entered(zone_1, area)

func _on_1_area_exited(area: Area2D) -> void:
  _on_camera_zone_exited(zone_1, area)

func _on_2_area_entered(area: Area2D) -> void:
  _on_camera_zone_entered(zone_2, area)

func _on_2_area_exited(area: Area2D) -> void:
  _on_camera_zone_exited(zone_2, area)

func _on_3_area_entered(area: Area2D) -> void:
  _on_camera_zone_entered(zone_3, area)

func _on_3_area_exited(area: Area2D) -> void:
  _on_camera_zone_exited(zone_3, area)

func _on_4_area_entered(area: Area2D) -> void:
  _on_camera_zone_entered(zone_4, area)

func _on_4_area_exited(area: Area2D) -> void:
  _on_camera_zone_exited(zone_4, area)
