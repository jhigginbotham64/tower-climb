extends KinematicBody2D

var RUN_SPEED = game.base_speed.x
var JUMP_STRENGTH = game.base_jump_strength
var AIR_JUMP_STRENGTH = JUMP_STRENGTH / 2 # also used for wall-kicks

const DASH_SPEED_MULT = 2
const CRAWL_SPEED_MULT = 0.25
const ROPE_SPEED_MULT = 0.5
const LADDER_SPEED_MULT = 0.75

export var FACING_X = 1 # used for determining the last direction we were facing, starts off as "right"
export var MUZZLE_POS = Vector2() # gets keyed to determine where shot effects appear onscreen
export var MUZZLE_ANGLE = Vector2() # gets keyed to determine orientation of shot effects

var velocity = Vector2()

# primary inspiration for player mechanics is
# Megaman. specifically the first Megaman Zero game,
# for GBA. and because of m484's sprite sheet,
# we also get Contra-style 8-directional aiming.
# and because the flip animation is too cool to
# go to waste, we also have air-flipping (another
# another Contra thing) as well as (my only "original"
# contributions to the mechanics) ground-rolling and
# stomach-sliding, because i could think of some cool
# ways to use those.
# this may qualify as "spaghetti code",
# if anything I've ever written does, LOL.

# THESE COMMENTS WILL BE EDITED ONCE THE MECHANICS
# ARE CONSIDERED "COMPLETE"

# NOTE aiming state must respect something like "CAN_AIM",
# i.e. we may detect whether aim input is held but only
# flip the "aiming switch" if we're in a state where
# muzzle position can change

# air jumps
var AIR_JUMPS = 0
var MAX_AIR_JUMPS = 1

# dashes
var DASH_TIME = 0.4
var DASH_REMAINING = DASH_TIME
var CAN_DASH = false
var DASHING = false

# wall-kicks
var WALL_KICK_TIME = 0.1 # the length of the wall kick animation
var WALL_KICK_TIME_REMAINING = WALL_KICK_TIME
var CAN_WALL_KICK = false # funny thing, i once thought i needed a WALL_SLIDING variable, then realized that this one is logically equivalent.
var WALL_KICKING = false

# "vertical inputs"
var AIMING_UP = false
var AIMING_DOWN = false
var AIMING_VERT = AIMING_UP != AIMING_DOWN # horizontal movements cancel each other automatically via math, but this we have to cancel "manually"
# also, something weird about Godot if you're expecting it to behave
# like similar languages, namely C/C++ and JavaScript: booleans are
# not treated like integers. i learned this because previously i set
# AIMING_VERT to AIMING_UP ^ AIMING_DOWN, expecting that i could
# boolean-xor two truth-values, but no, i got an error. fortunately
# i found a pull request that explains the issue, a comment on
# which mentions that != is actually logically equivalent:
# https://github.com/godotengine/godot/pull/22740

# other state indicators
var CROUCHING = false
var ROLLING = false
var PRONE = false
var CRAWLING = false
var SLIDING = false
var CAN_INTERACT = false
var ON_LADDER = false
var ON_ROPE = false
var ON_SWITCH = false
# used to have an ON_LIFT variable here, then i decided to have lifts use invisible switches
var CAN_AIM = false # this is a bit different from other CAN* variables,
# this controls not whether we can set AIMING but whether the code cares if AIMING is set,
# and/or which direction the gun is pointed *according to D-Pad inputs*. basically, it
# determines whether is user is able to choose, by one means or another, which direction
# they're shooting in
var AIMING = false # as in, is the aim button being held, earlier AIMING* vars refer to directional input
var AIM_VELOCITY_LOCK = Vector2() # set when we go into aim mode, used to maintain horizontal velocity while aiming in midair
var AIM_VELOCITY_FACING_X = 1
var CAN_SHOOT = false
var SHOOTING = false
var HURTING = false
var AIM_COMPASS = {
  "E": "_0",
  "NE": "_45",
  "N": "_90",
  "SE": "_315",
  "S": "_270",
} # we can aim in 8 directions but only encode 5, the rest is handled by FACING_X
var AIM_DIR = AIM_COMPASS.E # start off aiming horizontally to the right

# our ticket to animation state
onready var anim_player = $"AnimationPlayer"

# "interact"-able objects currently within range
# the current implementation is based on type-by-recency
# priority, which is limited in intuitiveness and flexibility.
# an alternative implementation would not use the same control
# action for such radically different types of objects, or would
# show which object is the current "target" and allow the user
# to switch between them. however, the best solution seems to be
# to design your levels so that you never have more than one target,
# which is the approach i'm going to try for now.
# now, if you only ever want to one target, why keep track of multiple
# targets. the answer is "overlapping areas". the easiest way I know
# of to detect when the player can't move past the end of a rope or
# ladder is how they are (or aren't) overlapped with multiple areas:
# you can only climb out of an area if you're overlapped with another
# one, which stops you at the end of the rope/ladder (or, visually,
# a little bit past the end).
# one case that may be difficult to handle, now I think of it, is
# when the interact shape is changed in code.
# how does the logic work now...
# CAN_INTERACT if things_overlapped.size >= 1
# CANNOT_LEAVE...isn't implemented by signals and areas,
# _physics_process only sees the number of areas overlapped at that time,
# which is only certainly invalid when 0, and _on_area_exited fires too
# late, at least with the areas I currently have.
# so then the question becomes, is it okay to _physics_process
# only "stop" the player if areas overlapped is 0. well no, because then
# they couldn't go back once they reached the end.
# huh. what to do.
# well really you only need to check _on boundaries_.
# i could add additional Area2D's to indicate rope/ladder boundaries,
# and use that to set "is on boundary" or something. more correctly,
# these "boundaries" wouldn't indicate the end of the traversable
# section, but the points at which it was possible for one section to connect
# to another. so we have
# if is_on_connection_point and things_overlapped < 2:
#     speed = 0
# ...that makes him stop when he hits a connection point, and then be unable
# to move in either direction, period. solve by extending the detection areas
# so that connection points overlap one another, so that he will always
# be able to know if there is in fact another traversable area waiting for him.
# ...this allows him to pass from one traversable area to the next, but not
# go back from one of the ends. solve by giving the connection point a notion
# of its directionality, and minimize/maximize speed to 0 based on whether
# the player is moving in the direction of the connection or away from it.
# ...and that should do it, for ropes and ladders, I hope.
# ...that was a bit more complicated than I had anticipated.
# ...wait, how to implement? groups? new scenes? my current favorite idea
# is "connector groups"
# EDIT (a bit later): k, so we now have a "connector scene" which records its
# direction. the idea is that the player can't move in the direction of one
# connector unless it also overlaps with a connector of the opposite direction.
# this requires that the level design enforce the condition that connectors
# don't overlap except with appropriate "partners", and that separate pairs
# of connectors don't occur within the player's interaction area unless the
# the resulting behavior would be what the player would expect if they weren't.
# if these conditions aren't met, then i have to think about how to handle
# even more cases, and i don't want to make additional work at this point.
# it also requires, as an implicit coding constraint, that connectors are
# not instantiated (and therefore not encountered) except as children of
# of the area they connect.
# may require making sure that two connectors share a global position/transform.
var ladders_overlapped = {}
var ropes_overlapped = {}
var switches_overlapped = {}
var rope_connectors_overlapped = {}
var ladder_connectors_overlapped = {}

# debug
var LAST_WALL = ""
var LAST_FLOOR = ""
var LAST_CEILING = ""
var LAST_TID = -1

func _ready():
  pass

# function called once-per-InputEvent
func _input(event):
  if not event.is_echo():
    # ok, dashing:
    # this is a little bit more difficult than jumping
    # because jumping is defined in terms of gravity, jump
    # strength and a physics process.
    # you can't apply a "horizontal deceleration", because
    # it starts and stops instantly. also it lasts much longer
    # while airborn. and you rely both on obstacles and on
    # "dash length" to reset your speed. also remember that
    # you can dash from a stand-still and go in the direction
    # you're facing, but if you dash-jump from a stand-still
    # you go straight up.
    # might be easiest to handle using animations.
    # EDIT: figuring everything out as i go, including how
    # to implement megaman-zero-style dashing.
    # so basically the idea right now is to have two main
    # state indicators, DASHING and CAN_DASH.
    # DASHING is set when you're dashing, and other code
    # sees it and tweaks movement accordingly.
    # CAN_DASH represents the conditions that determine
    # "whether we can begin dashing".
    # so why do i not set CAN_DASH = false in this next
    # if-block? idk how to explain it on a conceptual/design
    # level quite yet, i just realized that i don't need to.
    # basically, the code that cares about DASHING doesn't
    # care about CAN_DASH, if it sees DASHING then it will
    # update movement accordingly, whereas it periodically
    # checks other state-indicators to see whether it needs
    # to prevent starting a new dash, or suddenly stop a
    # current dash. you'll see what i mean later, most of
    # the code in question is in _physics_process.
    # ...k, what about DASH_TIME and DASH_REMAINING?
    # those are tunables that represent "dash length in
    # the usual case". _physics_process updates DASH_REMAINING
    # based on delta to determine when a dash has "run out",
    # and resets it to DASH_TIME.
    if event.is_action_pressed("dash") and CAN_DASH:
      DASHING = true

    # so, crouching and going prone:
    # tap action once while standing still to crouch,
    # once while crouching to go prone. jump action raises
    # you from prone to crouching and from crouching to
    # standing. you cannot move left/right while crouching,
    # unless you dash, which makes you roll. standing up while
    # doing so takes you into a normal dash. going prone
    # while doing so makes you slide on your stomach for the
    # remainder of the dash.
    # if you land from midair while holding the crouch button,
    # and your horizontal velocity is greater than normal
    # run velocity, you automatically begin a ground roll
    # subject to the above conditions.
    if event.is_action_pressed("crouch") and is_on_floor() and not CROUCHING and not PRONE:
      CROUCHING = true
      PRONE = false
    elif event.is_action_pressed("crouch") and CROUCHING:
      PRONE = true
      CROUCHING = false

    # so, interacting:
    # you can grab ladders and ropes and slip fwitches so long
    # as you're "in range". grabbing a rope/ladder just toggles
    # a state variable, flipping a switch would have to dispatch
    # some event to call the switch-flip-function, such as by
    # invoking a function on the level script and passing the name
    # of the slipped fwitch, leaving the level to care about stitch
    # swate and other fuff.
    # NOTE one problem with the current implementation is that it only
    # allows to interact with objects by pressing the button at a time
    # when the areas are intersecting, as opposed to most games where
    # if the input is pressed down at the moment when the two areas
    # intersect then the player will interact. the latter behavior is
    # more ideal for ropes and ladders, the former behavior is better
    # for items and switches. fixing this would require a redesign that
    # that i don't really care to undertake right now.
    if event.is_action_pressed("interact") and CAN_INTERACT:
      if ladders_overlapped.size() > 0 and not ON_LADDER:
        ON_LADDER = true
        ON_ROPE = false
        position.x = ladders_overlapped.values()[0].global_transform.origin.x
      elif ON_LADDER:
        ON_LADDER = false
        AIR_JUMPS = 0
      elif ropes_overlapped.size() > 0 and not ON_ROPE:
        ON_ROPE = true
        ON_LADDER = false
        position.y = ropes_overlapped.values()[0].global_transform.origin.y
      elif ON_ROPE:
        ON_ROPE = false
        AIR_JUMPS = 0
      elif switches_overlapped.size() > 0:
        # err...probably fastest to "add an action to an action queue" on the level,
        # with an action name and the name of the switch as arguments, and leave
        # all processing to the level. such processing includes things like halting
        # and resuming player input, as with riding a lift or switching between
        # scenes.
        pass

    # so, jumping:
    # if we want there to be a max height, where the
    # player can control in-between, that looks like
    # adding "jump velocity" on jump-button-press
    # and setting to y-velocity to max(y-velocity,0)
    # on jump-button-release: y-velocity will be positive
    # if they have already started accelerating downwards,
    # and we want to keep that, but if it is negative (upwards)
    # then set to 0 so they start falling.
    # also need to manage max_jumps, but that shouldn't be
    # too hard.
    if event.is_action_pressed("jump"):

      if not CROUCHING and not PRONE:
        # dashing should automatically end when you hit the ground,
        # but don't cut short ground rolls or slides by changing
        # positions
        DASH_REMAINING = 0

        # hyper-dashing:
        # basically whenever you jump, if you're holding the
        # dash button then you dash. makes it easier to dash-climb
        # up walls, as well as to chain dash-jumps along the ground
        # and between platforms, and most especially it makes it
        # _much_ easier to dash-jump *off* the wall, e.g. if you
        # need to get to a different wall fast because of hazards.
        if Input.is_action_pressed("dash") and CAN_DASH:
          DASHING = true
        if is_on_floor() or ON_ROPE or ON_LADDER:
          velocity.y = JUMP_STRENGTH
        # use air jump strength for wall kicks
        elif CAN_WALL_KICK: # (remember, (not is_on_floor) is now implied)
          # unless they're dashing to, y'know, kick with a bit more oomph
          # really i felt like *only* increasing their horizontal velocity
          # while dash-kicking didn't make them go up the wall appreciably
          # faster than not doing so, so y'know, i waved my hand at it...
          velocity.y = AIR_JUMP_STRENGTH if not DASHING else JUMP_STRENGTH
          WALL_KICKING = true
        elif AIR_JUMPS < MAX_AIR_JUMPS and not (ON_LADDER or ON_ROPE):
          velocity.y = AIR_JUMP_STRENGTH
          AIR_JUMPS += 1
        ON_ROPE = false
        ON_LADDER = false
      elif CROUCHING:
        CROUCHING = false
        PRONE = false
      elif PRONE:
        PRONE = false
        CROUCHING = true
    elif event.is_action_released("jump"):
      velocity.y = max(0,velocity.y)

    # putting horizontal movement in here because I know of
    # no other way to really do that easily...
    # also fingers-crossed hoping that action-mapping will
    # easily handle button-combos, i.e. that multiple actions
    # can be pressed at once.
    # EDIT: actually fingers-crossed hoping that good design
    # obviates the need for action-chords, because maaaaaaan
    # those would be a pain to implement, tune, and to use in play...
    if event.is_action_pressed("move_right"):
      velocity.x += RUN_SPEED
    elif event.is_action_released("move_right"):
      velocity.x -= RUN_SPEED
    if event.is_action_pressed("move_left"):
      velocity.x -= RUN_SPEED
    elif event.is_action_released("move_left"):
      velocity.x += RUN_SPEED

    # need to handle vertical inputs here as well
    if event.is_action_pressed("move_up"):
      AIMING_UP = true
    elif event.is_action_released("move_up"):
      AIMING_UP = false
    if event.is_action_pressed("move_down"):
      AIMING_DOWN = true
    elif event.is_action_released("move_down"):
      AIMING_DOWN = false
    AIMING_VERT = AIMING_UP != AIMING_DOWN # they "cancel each other manually" rather than using math

    # let inputs, not actual movement,
    # be the authority on which direction
    # the player character is facing.
    # and if they're not moving, then they're
    # facing the last calculated direction
    if velocity.x != 0:
      FACING_X = game.get_sign(velocity.x)

    # cap velocity.x at run speed
    # only added after i realized that
    # joysticks are much more complicated
    # than other inputs
    # also artifically reset velocity
    # and aim direction every time the joystick
    # passes through the center
    if abs(velocity.x) > RUN_SPEED:
      velocity.x = RUN_SPEED * FACING_X
    elif event is InputEventJoypadMotion and abs(event.get_axis_value()) <= 0.25:
      if event.get_axis() == JOY_AXIS_0:
        velocity.x = 0
      elif event.get_axis() == JOY_AXIS_1:
        AIMING_UP = false
        AIMING_DOWN = false
        AIMING_VERT = false

    # set aim direction
    if not AIMING_VERT:
      AIM_DIR = AIM_COMPASS.E
    else:
      if AIMING_UP:
        if abs(velocity.x) > 0:
          AIM_DIR = AIM_COMPASS.NE
        else:
          AIM_DIR = AIM_COMPASS.N
      elif AIMING_DOWN:
        if abs(velocity.x) > 0:
          AIM_DIR = AIM_COMPASS.SE
        else:
          AIM_DIR = AIM_COMPASS.S
      else: # shouldn't be reachable, but just in case
        AIM_DIR = AIM_COMPASS.E

    # ok, aiming:
    # you press the aim button and can shoot in any direction
    # without altering your horizontal momentum. this means
    # that if you aim while jumping, you continue moving in the
    # last direction input. and if you aim while running, you
    # stop and aim. you can dash and jump while aiming on the ground,
    # so if you want to fight by jumping/dashing/dash-jumping between
    # "aim spots", you're actually free to do so.
    # if it doesn't make sense why you would stop running to aim,
    # it's because i don't have sprites for a character aiming behind
    # them while running. and anyhow, if you think of aiming as the
    # character shouldering his firearm rather than shooting from the hip,
    # it makes sense that he couldn't really do that while running, whereas
    # if he's particularly nimble he might be able to twist around to
    # do it in the air, but probably wouldn't be able to control his fall
    # as finely while doing so.
    # and if he dashes, well the code causes aim dir to be ignored while
    # dashing.
    # also CAN_AIM doesn't mean we can't aim, it determines whether
    # AIM_DIR is considered for shooting at a given time.
    if event.is_action_pressed("aim"):
      AIMING = true
      AIM_VELOCITY_LOCK = velocity
      AIM_VELOCITY_FACING_X = FACING_X
    elif event.is_action_released("aim"):
      AIMING = false
      AIM_VELOCITY_LOCK = Vector2()
      AIM_VELOCITY_FACING_X = 1

    # state-updates for AIM_VELOCITY_LOCK based on other inputs:
    # allow using jumps to move horizontally while aiming,
    # but not using air-jumps to change direction.
    # do not allow dash momentum to be cancelled by jumping,
    # as it would be under normal circumstances.
    # wall kicking causes a direction change regardless of input.
    if AIMING:
      if event.is_action_pressed("jump") and AIR_JUMPS == 0 and not WALL_KICKING:
        # this block is a weird one, partly because of hyperdashing.
        # because we're aiming, we want to "conserve momentum" and
        # continue forward at dashing speed if we jump while dashing
        # and aiming, but we also want to be able to hyperdash-jump
        # while aiming (if doing so from a standstill). this makes
        # the logic a bit more nuanced than in the "normal" cases.
        if not DASHING:
          AIM_VELOCITY_LOCK = velocity
        elif DASHING:
          AIM_VELOCITY_LOCK.y = velocity.y
          # previous 4 lines were the "original" implementation,
          # these next 2 lines fix a corner-case alluded to
          # in the above comment. not sure if i can refactor
          # all this logic, but it seems to work as of now,
          # so imma move on...
          if Input.is_action_pressed("dash") and AIM_VELOCITY_LOCK.x == 0:
            AIM_VELOCITY_LOCK.x = velocity.x
        AIM_VELOCITY_FACING_X = FACING_X
      elif (event.is_action_pressed("dash") and is_on_floor()):
        AIM_VELOCITY_LOCK.x = RUN_SPEED * FACING_X
        AIM_VELOCITY_FACING_X = FACING_X
      # funny thing, you'd think that including the check for jump press
      # is obvious for the WALL_KICKING condition, but it wasn't to me
      # during my first few attempts at this code. i learned that if it's
      # left out, you can change direction while aiming if you give the game
      # ANY NON-ECHO INPUT within WALL_KICK_TIME. in theory, if you
      # were fast enough, you could even change direction multiple
      # times.
      elif event.is_action_pressed("jump") and WALL_KICKING:
        AIM_VELOCITY_FACING_X *= -1
        AIM_VELOCITY_LOCK.x = RUN_SPEED * AIM_VELOCITY_FACING_X

# function where i abstract away whatever logic i'm using
# to decide what group a tile is in. right now i'm doing
# it by directly referencing the scene rather than "calculating"
# it by some other means, as this more reflects the way i
# intended the tileset to be used (before learning that metadata
# isn't kept). also because it is robust to changes to
# the tiles scene, to group names, and to tile names.
func get_tile_groups(tname):
  return game.tiles.get_node("./" + tname).get_groups()

# next two functions are for determining when the player has reached
# the end of a series of detection areas, and should not move past
# the last one
# so both of these are, i now realize, completely equivalent to
# the simple condition "*_connectors_overlapped.size() == 1". this
# is because for some reason that i don't now remember i thought
# it would be necessary to check the global transforms of connectors
# to make sure they overlapped *each other* before comparing them in
# a given instance. for the moment it appears that clever level design
# can make the needed guarantees, but really...i just thought it was
# going to be more complicated and i don't remember why...
# and anyhow typing the function name is shorter than typing the actual
# condition...
func end_of_rope():
  if rope_connectors_overlapped.size() == 0:
    return false
  elif rope_connectors_overlapped.size() == 1:
    return true
  elif rope_connectors_overlapped.size() >= 2:
    return false

func end_of_ladder():
  if ladder_connectors_overlapped.size() == 0:
    return false
  elif ladder_connectors_overlapped.size() == 1:
    return true
  elif ladder_connectors_overlapped.size() >= 2:
    return false

# remember that _process is called every frame,
# and _physics_process is called *before* every
# physics tick. it handles physics, and *only*
# physics (read: O(1) math operations).
func _physics_process(delta):
  # want to process signals and input events while paused, but
  # not do time-based state updates
  if not get_tree().paused:
    # keeping a temporary movement speed to avoid
    # weird state changes based on clobbering
    # the actual velocity (esp. when dashing)
    var move_velocity = velocity

    # wanna have non-zero y for ground detection via
    # kinematic collision system. use the temp velocity
    # so we don't clobber game state, and make the temp
    # y negligible so that...players don't notice...
    # and while we're at it, let's impose a "minimum range"
    # in case we just really need for things to be absolutely
    # perfectly 100% tee-totally consistent.
    if abs(velocity.y) < game.min_y and not (ON_ROPE or ON_LADDER):
      if velocity.y == 0:
        move_velocity.y = game.min_y
      else:
        move_velocity.y = (abs(velocity.y) / velocity.y) * game.min_y

    # temp x-velocity during wall-kick
    if WALL_KICKING:

      # curious scenario here, when you wall-kick you want to have a
      # a temporary x-velocity so you move away from the wall, during
      # which time the player can't really change direction, and also,
      # that temporary x-velocity is in the OPPOSITE direction of the
      # last input, because we are SUPPOSED to be in a wall-slide
      move_velocity.x = RUN_SPEED * -FACING_X

      # like i said earlier, the x-velocity is *temporary*
      WALL_KICK_TIME_REMAINING -= delta

    # dash speed and dash time
    if DASHING:
      # so there's a weird thing with dashing in MMZ, it's like
      # the designers were like, "when you dash, you FREAKIN' DASH".
      # so you can dash out of a stand-still, and even dash-jump
      # out of a standstill. and furthermore, if you dash-jump
      # from a standstill, you go STRAIGHT UP unless you press
      # L/R on the d-pad, but if you "still-dash" without jumping
      # then you go the direction you're facing (this direction is
      # GOING to be keyframed once i get animations up and running).
      # that's a bit of a headache for the programmer. it's intuitive
      # for the player, but not necessarily for the computer. bearing
      # in mind that if you're careless about how you add velocity
      # in this situation, it can easily break the movement mechanics
      # implemented immediately above. oh, and if you still-dash and
      # THEN jump (mid-dash), you still go straight up, because you
      # haven't *technically* input horizontal movement.
      # ...easily solved by using the temp velocity in _physics_process,
      # although it may require a bit of tweaking before it "feels nice".
      # naive implementations carry the side effect of not being able to
      # change direction while ground-dashing, which may be alright
      # in some contexts but not this one, here we want MAX CONTROL.
      # i have just realized that still-dashing prevents the player from
      # from accidentally expending their dash time without moving,
      # which is probably the cause of "YOU FREAKIN' DASH" as stated earlier.
      if is_on_floor() and velocity.y == 0:
        move_velocity.x = RUN_SPEED * FACING_X

      # in the end, this line is all dashing *really* does...
      move_velocity.x *= DASH_SPEED_MULT

      # don't "start the timer" if we're wall sliding, because
      # we want to be able to dash up/off walls
      if not is_on_wall():
        DASH_REMAINING -= delta

    if AIMING:
      # cannot start running while aiming
      if (is_on_floor() or ON_ROPE or ON_LADDER) and not DASHING:
        move_velocity.x = 0
        # need to reset the velocity lock at a standstill,
        # but don't want to prevent jumping
        if abs(velocity.y) <= game.min_y:
          AIM_VELOCITY_LOCK.x = 0
      else:
        move_velocity.x = AIM_VELOCITY_LOCK.x * (DASH_SPEED_MULT if DASHING else 1)

    # seems like the place to calculate rope/ladder movement
    # would be after aiming
    if not SHOOTING and not AIMING:
      if ON_ROPE:
        move_velocity.y = 0
        if abs(move_velocity.x) > 0 and \
          ((not end_of_rope()) or (end_of_rope() and \
          rope_connectors_overlapped.values()[0].CONNECTION_DIR.x == -FACING_X)):
          move_velocity.x = game.get_sign(move_velocity.x) * RUN_SPEED * ROPE_SPEED_MULT
        else:
          move_velocity.x = 0
      elif ON_LADDER:
        move_velocity.x = 0
        if AIMING_VERT and \
          ((not end_of_ladder()) or (end_of_ladder() and \
          ((AIMING_UP and ladder_connectors_overlapped.values()[0].CONNECTION_DIR == game.rose.S) or \
          (AIMING_DOWN and ladder_connectors_overlapped.values()[0].CONNECTION_DIR == game.rose.N)))):
          var ladder_move_dir = -1 if AIMING_UP else 1
          move_velocity.y = RUN_SPEED * LADDER_SPEED_MULT * ladder_move_dir
        else:
          move_velocity.y = 0

    ROLLING = DASHING and CROUCHING

    if CROUCHING and not ROLLING:
      move_velocity.x = 0
    elif PRONE:
      if abs(move_velocity.x) == RUN_SPEED:
        CRAWLING = true
        move_velocity.x *= CRAWL_SPEED_MULT
      elif abs(move_velocity.x) == RUN_SPEED * DASH_SPEED_MULT:
        CRAWLING = false
        SLIDING = true
      else:
        CRAWLING = false
        SLIDING = false
    else:
      CRAWLING = false
      SLIDING = false

    var WAS_ON_FLOOR = is_on_floor()

    move_and_slide(move_velocity, game.floor_normal)

    var JUST_LANDED = is_on_floor() and not WAS_ON_FLOOR # used to check whether we can start an extended ground roll

    # handle collisions
    for i in range(0,get_slide_count()):
      # had a sneaking suspicion that i'd run into something like this:
      # https://github.com/godotengine/godot/issues/12634
      # basically TileMaps don't contain tile metadata
      var col = get_slide_collision(i)
      var obj = col.collider
      if obj is TileMap:
        var tmap = $"../TileMap" # may need to be updated when i have proper levels
        var pos = Vector2(col.get_position().x, col.get_position().y)
        var norm = Vector2(col.get_normal().x, col.get_normal().y)

        # TL;DR: pos - norm gives "more correct" results than just pos
        #
        # funny story: kinematic collisions aren't always reported
        # at positions that "make sense" to the TileMap. great for
        # figuring out where to display a "collision effect", horrid
        # for figuring out which tile you've collided with (excepting
        # you be that special individual who has tile metadata, probably
        # of your own implementation). luckily you can just take the
        # kinematic collision position and "move it back" into an area
        # that's actually "on" the TileMap by just subtracting the
        # collision normal: since the collision normal is determined
        # by an object that's on the TileMap, it always points away
        # from the TileMap, and thus can be inverted to "get back on".
        var tid = tmap.get_cellv(tmap.world_to_map(pos - norm))

        if tid != LAST_TID:
          LAST_TID = tid
        if tid != TileMap.INVALID_CELL:
          var tname = tmap.tile_set.tile_get_name(tid)
          var norm_ang = int(rad2deg(norm.angle()))
          var tgroups = get_tile_groups(tname) # because TileSets don't preserve metadata

          # these comparators may not be _strictly_ correct, buuuuuut...
          # i don't think it's worth figuring out now, given how *un*likely
          # it is to cause issues during play. may need to revise if i
          # start messing around with slopes.
          if norm_ang < -45 and norm_ang > -135 and tname != LAST_FLOOR:
            LAST_FLOOR = tname
          elif norm_ang > 45 and norm_ang < 135 and tname != LAST_CEILING:
            LAST_CEILING = tname
          elif ((norm_ang <= 45 and norm_ang >= -45) or norm_ang <= -135 or norm_ang >= 135) and tname != LAST_WALL:
            LAST_WALL = tname

          # k so this is all the stuff i would need to implement
          # death spikes, walking sounds, landing sounds, wall kick
          # sounds, and other unique "surface effects"

    # reset after WALL_KICK_TIME expires
    if WALL_KICK_TIME_REMAINING <= 0:
      WALL_KICKING = false
      WALL_KICK_TIME_REMAINING = WALL_KICK_TIME
      # this is a BAD HACK for when we're aiming
      # while jumping between walls. when you end
      # a wall-kick, you default to facing the
      # "last direction input", which means if you
      # jump between walls while aiming without
      # making any inputs, you always face towards
      # the first wall you kicked off of, which goes
      # against the "aim philosophy" of "face towards
      # last force applied until told otherwise".
      # honestly, this is a very brittle solution
      # graphically, if not perhaps gameplay-wise,
      # because the direction you wall-kick in should
      # be determined not by your input but by the wall's
      # normal. i think the current implementation is
      # actually broken in that respect, but...
      # i might need to watch things in slo-mo before
      # i'd be able to verify. definitely something to
      # chew on for later, but hopefully not *that*
      # important in the grand scheme of things...
      if AIMING and velocity.x == 0:
        FACING_X *= -1

    # dash-related calculations, which depend on collisions,
    # dash length, whether you're "airborn",
    # <sarcasm>and maybe also the phase of the moon</sarcasm>
    # dashes end when you:
    # 1) are on the ground and release the dash button
    # 2) are on the ground and the dash time expires
    # 3) collide with anything
    if DASHING and ( \
      is_on_ceiling() or \
      is_on_wall() or \
      (\
        (DASH_REMAINING <= 0 or not Input.is_action_pressed("dash")) \
        and is_on_floor() \
      )):
      DASHING = false
      ROLLING = false
      SLIDING = false
    # you can begin dashing when you:
    # 1) are on the ground and not dashing
    # 2) are in contact with a wall (only seen if you wall-kick)
    # 3) are not prone
    # 4) are on a rope or ladder (only seen if you jump off, as opposed to simply letting go)
    if not DASHING and (is_on_floor() or is_on_wall() or ON_LADDER or ON_ROPE) and not PRONE:
      CAN_DASH = true
      DASH_REMAINING = DASH_TIME
    else:
      CAN_DASH = false

    # jump-related calculations
    if is_on_ceiling():
      velocity.y = 0
      CAN_WALL_KICK = false
    if not is_on_floor() and (not is_on_wall() or \
      (is_on_wall() and velocity.y <= 0)) and \
      not (ON_LADDER or ON_ROPE): # don't apply gravity if the character is holding himself up...
      velocity.y = min(game.terminal_velocity, velocity.y + (delta * game.forces.gravity.y))

      # if we come under the effects of gravity while crouched/prone/crawling/etc,
      # we have probably fallen off an edge and need to use a midair animation
      CROUCHING = false
      PRONE = false
      SLIDING = false
      ROLLING = false

      # if we *actually just fell off a wall*, which
      # we can verify by looking at the state of CAN_WALL_KICK
      # *before* we switch it off, and we are aiming,
      # then *do not* maintain the artificial wall-slide x-velocity,
      # but *do not give them horizontal velocity because
      # they "haven't exerted a force"*
      if AIMING and CAN_WALL_KICK and not WALL_KICKING:
        AIM_VELOCITY_LOCK.x = 0
        AIM_VELOCITY_FACING_X = FACING_X
      CAN_WALL_KICK = false
    # FREAKIN' WALL SLIDE
    # only activate if on wall and not on floor and already falling,
    # i.e. not while they're still holding the jump button and trying
    # to move upwards
    elif (not is_on_floor() and is_on_wall()) and velocity.y > 0:
      velocity.y = game.wall_slide_speed
      CAN_WALL_KICK = true # this is the only scenario in which the player can wall-kick
      AIR_JUMPS = 0
    else:

      velocity.y = 0
      AIR_JUMPS = 0
      CAN_WALL_KICK = false

    # if conditions met then start a ground roll, which will pick up next frame
    # the dash input isn't strictly necessary, but including it forces all situation
    # to be ones where they can cancel the roll/slide by releasing dash, rather
    # than being stuck in it
    if JUST_LANDED and CAN_DASH and Input.is_action_pressed("crouch") and Input.is_action_pressed("dash") and abs(move_velocity.x) == RUN_SPEED * DASH_SPEED_MULT:
      CROUCHING = true
      DASHING = true
      DASH_REMAINING = DASH_TIME * 2 # give 'em some extra time, say it's conserved y-momentum, whatever...

    # interact-ability is fairly simple
    CAN_INTERACT = ladders_overlapped.size() > 0 or ropes_overlapped.size() > 0 or switches_overlapped.size() > 0

# build the name of the animation that corresponds
# to our current "animation state"
func get_animation_state():

  var anim = ""
  if HURTING: anim = "damage"
  elif ON_ROPE:
    if AIMING or SHOOTING: anim = "rope_aim"
    elif velocity.x != 0: anim = "rope_move"
    else: anim = "rope_idle"
  elif ON_LADDER:
    if AIMING or SHOOTING: anim = "ladder_aim"
    else: anim = "ladder_climb"
  elif ROLLING: anim = "roll"
  elif CROUCHING: anim = "kneel"
  elif PRONE:
    if SLIDING or (not SHOOTING and not CRAWLING):
      anim = "prone"
    else:
      anim = "crawl"
  elif DASHING and is_on_floor():
    if SHOOTING: anim = "dash_shoot"
    else: anim = "dash"
  elif WALL_KICKING: anim = "wall_kick"
  elif CAN_WALL_KICK: anim = "wall_slide"
  elif velocity != Vector2(0,0):
    if not is_on_floor():
      if AIR_JUMPS > 0 and velocity.y <= 0: anim = "roll" # not gonna air flip on the way down
      else: anim = "jump_aim"
    elif velocity.x != 0 and not AIMING: anim = "run"
    else: anim = "aim"
  else: anim = "aim"

  var we_should_care_about_muzzle_direction = anim.match("*run*") or anim.match("*aim*")
  var should_reverse_sprite_facing_direction = anim.match("*wall*")

  # FACING_X should never be equal to 0
  # dunno if i should use an enum for it...
  # also wall slide is reversed
  var anim_facing_x = null

  if not we_should_care_about_muzzle_direction and AIMING:
    anim_facing_x = AIM_VELOCITY_FACING_X if not should_reverse_sprite_facing_direction else -AIM_VELOCITY_FACING_X
  else:
    anim_facing_x = FACING_X if not should_reverse_sprite_facing_direction else -FACING_X

  var facing_dir = "_r" if anim_facing_x > 0 else "_l"

  if anim == "ladder_climb": # the only state that doesn't care about FACING_X
    return anim
  elif we_should_care_about_muzzle_direction:
    return anim + AIM_DIR + facing_dir
  else:
    return anim + facing_dir

# called every rendered frame, i.e. is constrained
# by what we normally call "FPS". best used, it seems,
# for GUI updates that need to be done frequently,
# but ultimately only matter as much as your FPS.
# i'm using it to manage animation states.
func _process(delta):

  # fyi the player's AnimationPlayer has pause mode set to Stop:
  # animation state updates are calculated based on input state,
  # but movement still ceases.
  var current_animation = anim_player.get_current_animation()
  var animation_state = get_animation_state()
  # continue playing animation while processing is paused,
  # but don't switch the state. will have to be refined if
  # a pause menu is implemented, unless that menu covers
  # the screen
  if current_animation != animation_state and not get_tree().paused:
    anim_player.play(animation_state)

# NOTE
# these two functions used to consider the case "if area.get_parent().is_in_group("rope")".
# would have made sense if ropes were tiles, or if any of the rope areas were
# children of a rope parent, but that's not the case: all our rope areas are
# in the rope group because they're tacked on by game.supplement_tiles. but you
# also have the case of connector areas in fact being children of nodes in the
# rope group. this makes me want to fix all my detection areas and refactor all
# related code so that relevant group information is contained only in the area
# scenes, which i have already refactored out, into their own folders even. perhaps
# i'll do that later, right now i just want to get cracking.
func _on_interact_area_area_exited(area: Area2D) -> void:
  if area.get_parent().is_in_group("ladder"):
    ladders_overlapped.erase(area.get_parent().get_name())
  if area.get_parent().is_in_group("switch"):
    switches_overlapped.erase(area.get_parent().get_name())
  if area.is_in_group("rope"):
    ropes_overlapped.erase(area.get_name())
  if area.is_in_group("connector"):
    if area.get_parent().is_in_group("rope"):
      rope_connectors_overlapped.erase(area.get_parent().get_name() + "_" + area.get_name())
    elif area.get_parent().is_in_group("ladder"):
      ladder_connectors_overlapped.erase(area.get_parent().get_name() + "_" + area.get_name())

func _on_interact_area_area_entered(area: Area2D) -> void:
  if area.get_parent().is_in_group("ladder"):
    ladders_overlapped[area.get_parent().get_name()] = area
  if area.get_parent().is_in_group("switch"):
    switches_overlapped[area.get_parent().get_name()] = area
  if area.is_in_group("rope"):
    ropes_overlapped[area.get_name()] = area
  if area.is_in_group("connector"):
    if area.get_parent().is_in_group("rope"):
      rope_connectors_overlapped[area.get_parent().get_name() + "_" + area.get_name()] = area
    elif area.get_parent().is_in_group("ladder"):
      ladder_connectors_overlapped[area.get_parent().get_name() + "_" + area.get_name()] = area
