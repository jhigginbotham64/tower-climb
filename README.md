# Tower Climb

A project where I learn about writing 2D action-platformers.

The title is my basic concept for this game: fully-implemented 2D side-scrolling action-platformer player mechanics ala some of my favorite games (it's going to be a cross-breed of Contra and Megaman Zero), where the player starts at the bottom of an obstacle course and "wins" when they reach the top.

Made possible chiefly by [Master484](https://opengameart.org/users/master484) for creating the OpenGunner assets and publishing them on [OGA](https://opengameart.org/) under a friendly Creative Commons license, and to [Rubonnek](https://opengameart.org/users/rubonnek) for collecting them [all in one place](https://opengameart.org/content/opengunner).

Will update with more specific resource links once I know what all I'm actually using.