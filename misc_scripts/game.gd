tool
extends Node2D
# game settings

# because that's what master484 designed
# the  opengunner asset library around
const base_size = { # opengunner
  "x": 50,
  "y": 50,
}

const base_speed = { # opengunner
  "x": base_size.x * 4, # 3 tiles / sec sounds reasonable to start off
  "y": base_size.y * 4,
}

const base_jump_strength = -300 # remember kids, negativity is positive! # opengunner

const forces = { # opengunner
  "gravity": Vector2(0,500), # same as default
}

const terminal_velocity = 600  # opengunner

const wall_slide_speed = 50 # there maybe should be acceleration during wall slides, but there isn't BECAUSE GAME # opengunner

# i can't tell you how much it hurts to be
# using this, but my current means of using
# kinematic collisions breaks without it
const min_y = 3 # opengunner

onready var tiles = preload("res://scenes/tiles/tiles.tscn").instance() # opengunner
onready var tileset = preload("res://scenes/tiles/tiles.tres") # opengunner

var tilename_detectionarea_map = { # opengunner
  "forest_light_vine": "vine",
  "forest_dark_vine": "vine",
  "forest_light_vine_end_left": "vine_end_left",
  "forest_dark_vine_end_left": "vine_end_left",
  "forest_light_vine_end_right": "vine_end_right",
  "forest_dark_vine_end_right": "vine_end_right",
 }

onready var detection_areas = { # opengunner
  "vine": preload("res://scenes/tiles/detection_areas/vine_area.tscn"),
  "vine_end_left": preload("res://scenes/tiles/detection_areas/vine_end_left_area.tscn"),
  "vine_end_right": preload("res://scenes/tiles/detection_areas/vine_end_right_area.tscn"),
 }

onready var player = preload("res://scenes/player/player.tscn") # idk if opengunner or jDirector
onready var camera = preload("res://scenes/misc/jCam2D.tscn") # ditto

var room_names = { # opengunner, but would probably need to be connected to jDirector
  "DEBUG_ARENA": "debug_arena",
  "BLUE_HALL_OF_DOORS": "blue_hall_of_doors",
  "FOREST_TO_MOUNTAIN": "forest_to_mountain",
  "SHOOTING_GALLERY": "shooting_gallery",
  "TUNNEL_ATRIUM": "tunnel_atrium",
  "TUNNEL_ENTRY_SHAFT": "tunnel_entry_shaft",
  "TUNNEL_EXIT_SHAFT": "tunnel_exit_shaft",
 }

# level sequence defined here
var next_rooms = { # opengunner, but again would need to be connected somehow
  room_names.DEBUG_ARENA: room_names.DEBUG_ARENA,
  room_names.BLUE_HALL_OF_DOORS: room_names.TUNNEL_ATRIUM,
  room_names.TUNNEL_ATRIUM: room_names.TUNNEL_ENTRY_SHAFT,
  room_names.TUNNEL_ENTRY_SHAFT: room_names.TUNNEL_EXIT_SHAFT,
  room_names.TUNNEL_EXIT_SHAFT: room_names.FOREST_TO_MOUNTAIN,
  room_names.SHOOTING_GALLERY: room_names.BLUE_HALL_OF_DOORS
 }

onready var rooms = { # opengunner
  room_names.DEBUG_ARENA: preload("res://scenes/rooms/debug/debug_arena.tscn"),
  room_names.BLUE_HALL_OF_DOORS: preload("res://scenes/rooms/blue_hall_of_doors/blue_hall_of_doors.tscn"),
  room_names.FOREST_TO_MOUNTAIN: preload("res://scenes/rooms/forest_to_mountain/forest_to_mountain.tscn"),
  room_names.SHOOTING_GALLERY: preload("res://scenes/rooms/shooting_gallery/shooting_gallery.tscn"),
  room_names.TUNNEL_ATRIUM: preload("res://scenes/rooms/tunnel_atrium/tunnel_atrium.tscn"),
  room_names.TUNNEL_ENTRY_SHAFT: preload("res://scenes/rooms/tunnel_entry_shaft/tunnel_entry_shaft.tscn"),
  room_names.TUNNEL_EXIT_SHAFT: preload("res://scenes/rooms/tunnel_exit_shaft/tunnel_exit_shaft.tscn"),
 }

var rose = { # i.e. compass rose # jDirector
  "N": Vector2(0,-1).normalized(), # up. derp. i had this as 0,1 earlier, it failed.
  "NE": Vector2(1,-1).normalized(),
  "E": Vector2(1,0).normalized(),
  "SE": Vector2(1,1).normalized(),
  "S": Vector2(0,1).normalized(),
  "SW": Vector2(-1,1).normalized(),
  "W": Vector2(-1,0).normalized(),
  "NW": Vector2(-1,-1).normalized()
 }

var floor_normal = rose.N # jDirector

func _ready(): # jDirector
  randomize() # never leave home without it

# about TileMaps in Godot:
# their power is limited by the fact that they don't allow you fine-grained
# control over tile position, they can't do Area detection, and you can't
# have "scripted tiles". basically TileMaps provide a library of basic tiles,
# rather than a library of arbitrary scenes, and tile placement is strictly
# confined to the grid. which means that graphics for special objects, such
# as ropes and ladders, has to be managed separately from the other tiles.
#
# problem: you need the whole to be artistically coherent and have pixel-perfect
# placement. doing this without a tilemap is somewhat difficult.
#
# solution: run a script over the TileMap to add missing functionality based
# on the TileMap. i.e. for each rope tile, add detection area; for each snake
# hole, add enemy snake spawn point; etc.
#
# this would have to be done in the _ready function of a level, or else,
# run against the level by a higher-level "level loader" (which would
# probably be the ideal situation). this would also be a good place to
# polish or even "generate" finished content for which the level scenes
# contain only shorthand descriptions, such as, for instance, I didn't want
# to repeatedly instantiate ladders but rather just say what position the
# top is at and how long that particular ladder is, and add in the sections
# at load-time (although that specific case might be better handled by the
# _ready function of a dedicated higher-level "ladder loading scene").
#
# another thing such a function could do would be to fill in location-specific
# metadata so that it wouldn't have to be calculated at run-time, such as what
# groups the tiles at a given location belong to. gotta be careful about premature
# optimization tho, that kinda thing would need some very carefully-considered
# data structures in order to work properly.
#
# if this function takes too long, it might be good to add a loading screen.
func supplement_tiles(level_node): # really more opengunner, but...idk maybe eventually a jSet feature?
  for node in level_node.get_children():
    if node is TileMap:
      for tilename in tilename_detectionarea_map.keys():
        for tile in node.get_used_cells_by_id(tileset.find_tile_by_name(tilename)):
          var tilepos = node.map_to_world(tile)
          var rope_area = detection_areas[tilename_detectionarea_map[tilename]].instance()
          rope_area.position = tilepos + (node.cell_size / 2) # needs to be centered
          rope_area.add_to_group("rope")
          level_node.add_child(rope_area)

const scroll_speed = 800 # idk if opengunner or jDirector

## GAME STATE MANAGEMENT
# i was tempted to include level_name as a parameter for these,
# but that would be redundant, whoever's handling this signal should
# already know which level is loaded
signal please_load(args) # jdirector
# lose condition met, not quite the same as
# end-of-level and thus might be handled differently
signal game_over(args) # jdirector
# realized that the most effective way to do start/end camera transitions
# was to have separate signals for each, and also to move it out of the
# level code. makes things a lot simpler.
signal scene_ready(args) # jdirector
# level has finished somehow, handler should introspect on level state
# and figure out what to do next
signal scene_end(args) # jdirector

# dictionary for shorthand signal names, because i don't like
# having to exactly type a function name without auto-complete
# any more times than i absolutely have to
var signals = { # jdirector
  "LOAD": "please_load",
  "GAME_OVER": "game_over",
  "SCENE_READY": "scene_ready",
  "SCENE_END": "scene_end",
 }

# lookup table for signal names and aliases
var signal_table = { # jdirector
  signals.LOAD: signals.LOAD,
  signals.GAME_OVER: signals.GAME_OVER,
  signals.SCENE_READY: signals.SCENE_READY,
  signals.SCENE_END: signals.SCENE_END,
  "LOAD": signals.LOAD,
  "GAME_OVER": signals.GAME_OVER,
  "SCENE_READY": signals.SCENE_READY,
  "SCENE_END": signals.SCENE_END
 }

# please_load is really the only signal that could potentially do
# wildly different things under different things, the following
# structure is my starting-point for...whatever system i end up
# using to define what load requests are possible and what they do.
# really the most important thing it could potentially do would be
# to define the argument types that a load handler could expect.
# idk what that would look like quite yet, i need to learn more about
# how exactly "Classes" work in Godot.
var load_requests = { # jdirector
  "NEXT_SCENE": "NEXT_SCENE"
 }

# nodes that have access to this singleton don't have access to its
# signals because of the way signal declaration/connection works in Godot,
# so i expose the signal through this function. arguments can be passed by
# array for now to keep the definitions simple.
func emit_game_signal(sig, args): # jdirector
  emit_signal(signal_table[sig], args)

# previous function combined with signal tables, which did not exist when
# i first wrote that function, make the following function desirable
func connect_game_signal(sig, obj, handler): # jdirector
  connect(signal_table[sig], obj, handler)

# other helper stuff

# tired of repeating this
func get_sign(num): # LOL jdirector
  return 0 if num == 0 else abs(num) / num

const alpha_max = 255 # jdirector
const alpha_min = 0 # jdirector

func bounded_alpha(num) -> int: # jdirector
  if num > alpha_max: return alpha_max
  elif num < alpha_min: return alpha_min
  else: return num
